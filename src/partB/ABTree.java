package partB;

import java.util.Enumeration;
import java.util.Random;
import java.util.Vector;

/*
 * This class represents a general balanced (a,b) tree, with a>=2 and b>=2*a-1.
 */
public class ABTree {
	static int totalNumberOfSplits = 0;	// the total number of splits actions
	static int totalNumberOfShares = 0;	// the total number of share actions
	static int totalNumberOfFuses = 0;		// the total number of fuse actions
	static int totalNumberOfSplitsInGivenHeight = 0;	// the total number of splits actions done in a given level
	static int totalNumberOfSharesInGivenHeight = 0;	// the total number of share actions done in a given level
	static int totalNumberOfFusesInGivenHeight = 0;	// the total number of fuse actions done in a given level
	static int testLevel = 4;

	private final int minChilds;			// minimum number of children for each node except root
	private final int maxChilds;			// maximum number of children for each node
	private int height;					// the tree's height
	private Node root;						// the root node of the tree
	private Vector<Node> internalNodes;	// stores all the internal nodes of the tree for printing usage

	/*
	 * Constructor. Constructs an empty (a,b) tree with the given value
	 * for a and b. If a is smaller than 2, is set to 2 and if b is smaller
	 * than 2*a-1, is set to 2*a-1, with the given value for a or the fixed
	 * one if it was wrong.
	 */
	public ABTree(int a, int b) {
		if(a < 2) {
			System.out.println("Warning: a can't be smaller than 2! Setting a to 2...");
			minChilds = 2;
		}
		else
			minChilds = a;
		if(b < 2*minChilds-1) {
			int new_b = (2*minChilds-1);
			System.out.println("Warning: b can't be smaller than "+new_b+"! Setting b to "+new_b+"...");
			maxChilds = new_b;
		}
		else
			maxChilds = b;
		height = 0;
		root = new Node();
		root.isLeaf = true;
		internalNodes = new Vector<Node>(0,1);
		internalNodes.addElement(root);
	}

	/*
	 * Searches for the given value in the tree. If the value
	 * is in the tree, the (leaf) node which contains the
	 * value is returned. else the root node is returned which
	 * means that the value isn't in the tree.
	 */
	private Node search(Integer value) {
		Node currentNode = root;
		//System.out.println("Searching for value "+value+"...");
		while(!currentNode.isLeaf) {
			for(int i=0; i<=currentNode.keys.size(); i++) {
				int key1;
				int key2;
				if(i == 0) {
					key1 = Integer.MIN_VALUE;
					key2 = currentNode.keys.elementAt(i).intValue();
				}
				else if(i < currentNode.keys.size()) {
					key1 = currentNode.keys.elementAt(i-1).intValue();
					key2 = currentNode.keys.elementAt(i).intValue();
				}
				else {
					key1 = currentNode.keys.elementAt(i-1).intValue();
					key2 = Integer.MAX_VALUE;
				}
				if(key1 < value && value <= key2) {
					currentNode = currentNode.children.elementAt(i);
					break;
				}
			}
		}
		return currentNode;
	}

	/*
	 * Splits the node given to 2 new nodes, to maintain the
	 * properties of the (a,b) tree.
	 */
	void split(Node node) {
		totalNumberOfSplits++;
		if(node.depth == testLevel)
			totalNumberOfSplitsInGivenHeight++;
		int indexOfFirstRightChild = (maxChilds+1)/2;
		Node leftNode = new Node();
		Node rightNode = new Node();
		Node middleChild = node.children.get(indexOfFirstRightChild);
		Integer middleKey = node.keys.get(indexOfFirstRightChild-1);
		// set children
		while(node.children.firstElement() != middleChild) {
			Node currentChild = node.children.remove(0);
			currentChild.parentNode = leftNode;
			leftNode.children.addElement(currentChild);
		}
		while(node.children.size() > 0) {
			Node currentChild = node.children.remove(0);
			currentChild.parentNode = rightNode;
			rightNode.children.addElement(currentChild);
		}
		// set keys
		while(node.keys.firstElement() != middleKey) {
			leftNode.keys.addElement(node.keys.remove(0));
		}
		while(node.keys.size() > 1) {
			rightNode.keys.addElement(node.keys.remove(1));
		}
		leftNode.isLeaf = node.isLeaf;
		rightNode.isLeaf = node.isLeaf;
		rightNode.depth = node.depth;
		leftNode.depth = node.depth;
		if(node == root) {
			root = new Node();
			root.keys.addElement(node.keys.remove(0));
			root.children.addElement(leftNode);
			root.children.addElement(rightNode);
			leftNode.parentNode = root;
			rightNode.parentNode = root;
			internalNodes.insertElementAt(root, 0);
			internalNodes.insertElementAt(leftNode, 1);
			internalNodes.insertElementAt(rightNode, 2);
			height++;
			// set new depths for all nodes
			for(int i=1; i<internalNodes.size(); i++) {
				internalNodes.elementAt(i).depth++;
				if(internalNodes.elementAt(i).depth == height-1) {
					for(int j=0; j<internalNodes.elementAt(i).children.size(); j++)
						internalNodes.elementAt(i).children.elementAt(j).depth++;
				}
			}
			internalNodes.remove(node);
		}
		else {
			Node parent = node.parentNode;
			leftNode.parentNode = parent;
			rightNode.parentNode = parent;
			int index1 = parent.children.indexOf(node);
			int index2 = internalNodes.indexOf(node);
			internalNodes.insertElementAt(leftNode, index2);
			internalNodes.insertElementAt(rightNode, index2+1);
			parent.keys.insertElementAt(node.keys.remove(0), index1);
			parent.children.insertElementAt(leftNode, index1);
			parent.children.insertElementAt(rightNode, index1+1);
			parent.children.remove(node);
			internalNodes.remove(node);
			if(parent.children.size() == maxChilds+1) {
				///System.out.println("No more children permitted! => splitting...");
				split(parent);
			}
		}
	}

	/*
	 * Inserts a new element in the tree, only if it's not already there.
	 * Returns true if the element inserted successfully, false otherwise.
	 * First searches for the specific element. If the element (value) is
	 * already in a leaf of the tree, nothing is inserted and the method
	 * returns false else the value is inserted and the method returns true.
	 * Also during the insertion of the new leaf, a check is done if a
	 * split action is required and called if it is.
	 */
	boolean insert(Integer value) {
		boolean valueInserted = false;
		if(root.keys.size() == 0 && root.children.size() == 0)	 {		// adding the very first value in the tree
			root.isLeaf = false;
			height++;
			Node newLeaf = new Node();
			newLeaf.parentNode = root;
			newLeaf.value = value;
			newLeaf.isLeaf = true;
			newLeaf.depth = height;
			root.children.addElement(newLeaf);
		}
		else if(root.keys.size() == 0 && root.children.size() == 1)		// adding the second value in the tree
			root.children.firstElement().addNewLeaf(value);
		else {
			Node node = search(value);
			if(node.value != null && node.value != value.intValue()) {		// value isn't in tree, insert it
				valueInserted = true;
				node.addNewLeaf(value);
			}
		}
		return valueInserted;
	}

	/*
	 * Share action that came up from a deletion after which
	 * the node left with minChilds-1. A sibling with more than
	 * minChilds gives one of its children to the node.
	 */
	void share(String str, Node sibling, Node incompleteNode, int incompleteNodeIndex) {
		totalNumberOfShares++;
		if(sibling.depth == testLevel)
			totalNumberOfSharesInGivenHeight++;
		if(str.equalsIgnoreCase("left")) {
			//System.out.println("Left sharing....");
			Node childToShare = sibling.children.remove(sibling.children.size()-1);
			Integer rightmostKey = sibling.keys.remove(sibling.keys.size()-1);
			sibling.parentNode.keys.insertElementAt(rightmostKey, incompleteNodeIndex);
			Integer parentKey = sibling.parentNode.keys.remove(incompleteNodeIndex+1);
			childToShare.parentNode = incompleteNode;
			incompleteNode.children.insertElementAt(childToShare, 0);
			incompleteNode.keys.insertElementAt(parentKey, 0);
		}
		else {
			//System.out.println("Right sharing....");
			Node childToShare = sibling.children.remove(0);
			Integer leftmostKey = sibling.keys.remove(0);
			sibling.parentNode.keys.insertElementAt(leftmostKey, incompleteNodeIndex);
			Integer parentKey = sibling.parentNode.keys.remove(incompleteNodeIndex+1);
			childToShare.parentNode = incompleteNode;
			incompleteNode.children.insertElementAt(childToShare, incompleteNode.children.size());
			incompleteNode.keys.insertElementAt(parentKey, 0);
		}
	}

	/*
	 * Fuse action that came up from a deletion after which
	 * the node left with minChilds-1. If a share action can't be done
	 * a fuse action must be done. A sibling with exactly minChilds
	 * merges with the node (with the minChilds-1 children) to a new
	 * node with 2*minChilds-1 children.
	 */
	Node fuse(String str, Node sibling, Node incompleteNode, int incompleteNodeIndex) {
		totalNumberOfFuses++;
		if(sibling.depth == testLevel)
			totalNumberOfFusesInGivenHeight++;
		if(str.equalsIgnoreCase("left")) {
			//System.out.println("Left fusing....");
			while(incompleteNode.children.size() > 0) {
				Node firstChild = incompleteNode.children.remove(0);
				firstChild.parentNode = sibling;
				sibling.children.addElement(firstChild);
			}
			sibling.keys.addElement(sibling.parentNode.keys.remove(incompleteNodeIndex));
			while(incompleteNode.keys.size() > 0) {
				sibling.keys.addElement(incompleteNode.keys.remove(0));
			}
		}
		else {
			//System.out.println("Right fusing....");
			while(incompleteNode.children.size() > 0) {
				Node lastChild = incompleteNode.children.remove(incompleteNode.children.size()-1);
				lastChild.parentNode = sibling;
				sibling.children.insertElementAt(lastChild, 0);
			}
			sibling.keys.insertElementAt(sibling.parentNode.keys.remove(incompleteNodeIndex), 0);
			while(incompleteNode.keys.size() > 0) {
				sibling.keys.insertElementAt(incompleteNode.keys.remove(incompleteNode.keys.size()-1), 0);
			}
		}
		sibling.parentNode.children.removeElement(incompleteNode);
		internalNodes.removeElement(incompleteNode);
		if(root.children.size() == 1) {
			//System.out.println("root has only 1 child!");
			Node newRoot = root.children.firstElement();
			newRoot.parentNode = null;
			newRoot.depth--;
			root = newRoot;
			internalNodes.removeElementAt(0);
			internalNodes.setElementAt(root, 0);
			height--;
			// set new depths for all nodes
			for(int i=1; i<internalNodes.size(); i++) {
				internalNodes.elementAt(i).depth--;
				if(internalNodes.elementAt(i).depth == height-1) {
					for(int j=0; j<internalNodes.elementAt(i).children.size(); j++)
						internalNodes.elementAt(i).children.elementAt(j).depth--;
				}
			}
		}
		//printTree();
		return sibling;
	}

	/*
	 * Deletes an element from the tree, only if it's already there.
	 * Returns true if the element deleted successfully, false otherwise.
	 * First searches for the specific element. If the element (value) is
	 * already in a leaf of the tree, it is deleted and the method
	 * returns true else nothing is deleted and the method returns false.
	 * Also during the deletion of the new leaf, a check is done if a
	 * repair action (share or fuse) is required and called if it is.
	 */
	boolean delete(Integer value) {
		boolean valueDeleted = false;
		if(root.children.size() == 1 && root.children.firstElement().value == value.intValue()) {
				// root has only one child that need to be removed
				System.out.println("Emptying tree...");
				root.children.removeElementAt(0);
				root.isLeaf = true;
				height--;
				return true;
		}
		else if(root.children.size() > 1) {
			Node node = search(value);
			if(node.value != null && node.value == value.intValue()) {		// value is in tree, delete it
				Node parent = node.parentNode;
				int index = parent.children.indexOf(node);
				if(index == parent.children.size()-1)
					index--;
				parent.children.removeElement(node);
				parent.keys.remove(index);
				if(root.children.size() == 1) {		// root left with one child and no keys
					return true;
				}
				if(parent.children.size() < minChilds) {
					Node mergedNode;
					do {		// do as many repair actions as required until tree properties are restored
						Node parentOfParent = parent.parentNode;
						int index2 = parentOfParent.children.indexOf(parent);
						Node leftSibling = null;
						Node rightSibling = null;
						mergedNode = null;
						int leftSiblingNumOfChildren = 0;
						int rightSiblingNumOfChildren = 0;
						if(index2-1 >= 0) {
							leftSibling = parentOfParent.children.get(index2-1);
							leftSiblingNumOfChildren = leftSibling.children.size();
						}
						if(index2+1 < parentOfParent.children.size()) {
							rightSibling = parentOfParent.children.get(index2+1);
							rightSiblingNumOfChildren = rightSibling.children.size();
						}
						if(leftSiblingNumOfChildren > minChilds && leftSiblingNumOfChildren >= rightSiblingNumOfChildren)
							share("left", leftSibling, parent, index2-1);
						else if(rightSiblingNumOfChildren > minChilds)
							share("right", rightSibling, parent, index2);
						else if(leftSibling != null)
							mergedNode = fuse("left", leftSibling, parent, index2-1);
						else
							mergedNode = fuse("right", rightSibling, parent, index2);

						if(mergedNode != null)		// a fuse was done
							parent = mergedNode.parentNode;
					} while(mergedNode != null && parent != null && parent.children.size() < minChilds);
				}
				valueDeleted = true;
				//printTree();
			}
		}
		return valueDeleted;
	}

	/*
	 * Return the number of stored elements in the tree.
	 * These are the values stored in all the leafs of the tree.
	 */
	int getNumOfStoredValues() {
		int numOfStoredValues = 0;
		for(int i=internalNodes.size()-1; i>=0; i--) {
			if(internalNodes.elementAt(i).depth < height-1)
				break;
			numOfStoredValues += internalNodes.elementAt(i).children.size();
		}
		return numOfStoredValues;
	}

	/*
	 * Print the tree in some way. Mostly (used) for
	 * debugging.
	 */
	void printTree() {
		int level = 1;
		int indexOfLastLevel = -1;
		//System.out.println("\ninternalNodes size = "+internalNodes.size());
		//System.out.println("tree height = "+height);
		System.out.print("\n(root) level 0:\t\t");
		for(Enumeration<Node> e = internalNodes.elements(); e.hasMoreElements();) {
			Node node = e.nextElement();
			if(indexOfLastLevel == -1 && node.depth == height-1)
				indexOfLastLevel = internalNodes.indexOf(node);
			if(node.depth >= level) {
				System.out.print("\n       level "+level+":\t\t");
				level++;
			}
			System.out.print("|-");
			for(Enumeration<Integer> e2 = node.keys.elements(); e2.hasMoreElements();) {
				System.out.print(e2.nextElement()+"-");
			}
			System.out.print("| ");
		}
		if(height > 0 && indexOfLastLevel != -1) {
			System.out.print("\n(leaf) level "+height+":\t\t");
			for(int i=indexOfLastLevel; i<internalNodes.size(); i++) {
				System.out.print("|-");
				for(Enumeration<Node> e = internalNodes.elementAt(i).children.elements(); e.hasMoreElements();) {
					System.out.print(e.nextElement().value+"-");
				}
				System.out.print("| ");
			}
		}
		System.out.println("\n");
	}

	/*
	 * Inner class that represents an node of the tree.
	 */
	private class Node {
		private Vector<Integer> keys;		// the keys of the node
		private Integer value;				// value is set only if the node is a leaf
		private Node parentNode;			// the parent node of this node
		private Vector<Node> children;		// the children nodes of this node
		private boolean isLeaf;			// is set to true only if the node is a leaf
		private int depth;					// the depth of the node

		private Node() {
			keys = new Vector<Integer>(maxChilds-1);
			value = null;
			parentNode = null;
			children = new Vector<Node>(maxChilds);
			isLeaf = false;
			depth = 0;
		}

		private void addNewLeaf(Integer value) {
			Node newLeaf = new Node();
			newLeaf.parentNode = this.parentNode;
			newLeaf.value = value;
			newLeaf.isLeaf = true;
			newLeaf.depth = height;
			int index = (this.parentNode).children.indexOf(this);
			if(value < this.value) {
				this.parentNode.children.insertElementAt(newLeaf, index);
				this.parentNode.keys.insertElementAt(value, index);
			}
			else {
				this.parentNode.children.insertElementAt(newLeaf, index+1);
				this.parentNode.keys.insertElementAt(this.value, index);
			}
			if(this.parentNode.children.size() == maxChilds+1) {
				//System.out.println("No more children permitted! => splitting...");
				split(this.parentNode);
			}
		}
	}

	public static void main(String[] args) {
		ABTree twoFour = null;
		Random random = new Random();
		int range = 100000;		// 0-99999

		// uncomment to test-print a small tree
//		twoFour = new ABTree(2,4);
//		for(int i=1; i<=15; i++)
//			twoFour.insert(i);
//		twoFour.printTree();
//		for(int i=1; i<=15; i++)
//			twoFour.delete(i);
//		twoFour.printTree();

		// ypoerothma 1
		for(int n=10; n<=100000; n*=10) {
			twoFour = new ABTree(2,4);
			for(int i=1; i<=n; i++)
				twoFour.insert(new Integer(random.nextInt(range)));
			System.out.println("The tree has "+twoFour.getNumOfStoredValues()+" stored values and its height = "+twoFour.height);
		}

		// ypoerothma 2
		//twoFour = new ABTree(2,4);
		int numOfInsertions = 0;
		int numOfDeletions = 0;
		for(int n=5; n<=50000; n*=10) {
			for(int i=1; i<=n; i++) {
				if(twoFour.insert(new Integer(random.nextInt(range))))
					numOfInsertions++;
				if(twoFour.delete(new Integer(random.nextInt(range))))
					numOfDeletions++;
			}
			System.out.println("\n\nTotal number of insertions and deletions tried = "+(2*n));
			System.out.println("Total number of insertions and deletions actually done = "+(numOfInsertions+numOfDeletions));
			System.out.println(numOfInsertions+" new elements inserted in tree and "+numOfDeletions+" elements deleted from tree");
			System.out.println("The tree has "+twoFour.getNumOfStoredValues()+" stored values and its height = "+twoFour.height);
			System.out.println("Total number of splits = "+totalNumberOfSplits);
			System.out.println("Total number of shares = "+totalNumberOfShares);
			System.out.println("Total number of fuses = "+totalNumberOfFuses);
			System.out.println("Total cost of insertions and deletions = "+(totalNumberOfSplits+totalNumberOfShares+totalNumberOfFuses));
			System.out.println("Total number of splits in level "+testLevel+" = "+totalNumberOfSplitsInGivenHeight);
			System.out.println("Total number of shares in level "+testLevel+" = "+totalNumberOfSharesInGivenHeight);
			System.out.println("Total number of fuses in level "+testLevel+" = "+totalNumberOfFusesInGivenHeight);
			System.out.print("Total cost of insertions and deletions in level "+testLevel+" = ");
			System.out.println(totalNumberOfSplitsInGivenHeight+totalNumberOfSharesInGivenHeight+totalNumberOfFusesInGivenHeight);
		}

		// ypoerothma 3
		System.out.println("\n\nThe tree has "+twoFour.getNumOfStoredValues()+" stored values and its height = "+twoFour.height);
		int elementsFound = 0;
		Node node = null;
		for(int n=10; n<=10000; n*=10) {
			for(int i=1; i<=n; i++) {
				Integer elementToSearch = new Integer(random.nextInt(range));
				node = twoFour.search(elementToSearch);
				if(node.value != null && node.value == elementToSearch.intValue())
					elementsFound++;
			}
			System.out.println("\nNumber of searches = "+n);
			System.out.println("Number of elements found in tree = "+elementsFound);
		}
	}
}
