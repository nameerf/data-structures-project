package partA.tape;

import java.util.Enumeration;
import java.util.Vector;

import partA.heap.MinHeap;
import partA.heap.HeapSort;

/*
 * This class is used to test external sorting using (simulated) magnetic tapes.
 */
public class ExternalSorting {

	static int N;								// the size of input to be sorted
	static int M = 10;							// the amount of available memory
	static int p = 2;								// the number of tapes used as input tapes
	static int numOfTapes = 2*p;				// the number of all tapes used (input and output tapes)
	static int totalNumberOfPasses;			// the total number of passes of the algorithm
	static int totalNumberOfRuns;				// the total number of runs of the algorithm
	static int totalNumberOfMemoryActions;		// the total number of actions performed in memory
	static int totalNumberOfTapeActions;		// the total number of actions performed by all tapes

	/*
	 *  main method
	 */
	public static void main(String[] args) throws Exception {
		for(N=10*M; N<=100000; N*=10) {
			System.out.println("##################### Size of input = "+N+" elements (integers) #####################");
			System.out.println("##################### Amount of available memory = "+M+" elements ####################");
			System.out.println("############################### Number of tapes = "+numOfTapes+"  ###############################\n");
			System.out.println("************************** Simple External Sorting ***************************\n");
			doExternalSorting(initializeTapesAndCounters(), "simple");
			System.out.println("\n");
			System.out.println("******************* Replacement Selection External Sorting *******************\n");
			doExternalSorting(initializeTapesAndCounters(), "");
			System.out.println("\n#####################################################################################\n");
		}
	}

	/*
	 * Initialization of tapes and counters. The input is written
	 * in tape 'p' every time.
	 */
	public static Tape[] initializeTapesAndCounters() {
		Tape[] tapes = new Tape[numOfTapes];
		int[] input = HeapSort.createArrayWithRandomIntegers(N);
		for(int i=0; i<numOfTapes; i++)
			tapes[i] = new Tape(N);
		for(int i=0; i<N; i++)
			tapes[p].writeHead(input[i]);
		totalNumberOfPasses = 0;
		totalNumberOfRuns = 0;
		totalNumberOfMemoryActions = 0;
		totalNumberOfTapeActions = 0;

		return tapes;
	}

	/*
	 * This method calls the methods responsible for the phase A (both forms)
	 * and B of the external sorting algorithm. Also prints useful stats
	 * about the algorithm.
	 */
	public static void doExternalSorting(Tape[] tapes, String form) throws Exception {
		long startTimeMs = System.currentTimeMillis();
		if(form.equals("simple"))
			phaseASimple(tapes);
		else
			phaseAReplacementSelection(tapes);
		//uncomment only for small size of input
//		System.out.println("---------- Tapes after phase A ----------");
//		printTapes(tapes);
		tapes[p].eraseBlocksEndPoints();
		phaseB(tapes);
		long taskTimeMs = System.currentTimeMillis() - startTimeMs;
		//uncomment only for small size of input
//		System.out.println("---------- Tapes after phase B ----------");
//		printTapes(tapes);
		calculateTotalNumOfTapeActions(tapes);
		System.out.println("Total number of memory actions = "+totalNumberOfMemoryActions);
		System.out.println("Total number of tape actions = "+totalNumberOfTapeActions);
		System.out.println("Total number of passes = "+totalNumberOfPasses);
		System.out.println("Total number of runs = "+totalNumberOfRuns);
		System.out.println("Total time elapsed (in sec) = "+taskTimeMs/1000);
	}

	/*
	 * The simple form of the A phase of the the external sorting algorithm.
	 */
	public static void phaseASimple(Tape[] tapes) throws Exception {
		int[] block = new int[M];
		MinHeap heap;
		boolean perfectDivision = (Tape.TAPE_LENGTH%M == 0) ? true : false;
		tapes[p].roll(tapes[p].headPointer);
		int blockNum = 0;
		boolean lastElem = false;
		boolean endOfInput = false;
		while(tapes[p].headPointer <= Tape.RIGHT_EDGE && !endOfInput) {
			block[tapes[p].headPointer%M] = tapes[p].readHead();
			totalNumberOfMemoryActions++;		// read an element from tape to memory
			if(!perfectDivision && lastElem) {
				int[] temp_block = block;
				block = new int[Tape.TAPE_LENGTH%M];
				System.arraycopy(temp_block, 0, block, 0, block.length);
				heap = HeapSort.heapSort(new MinHeap(block));
				writeBlockToTape(tapes[blockNum%p],HeapSort.reverseArray(heap.getHeapArray()));
			}
			else if(tapes[p].headPointer%M == 0 || lastElem) {
				heap = HeapSort.heapSort(new MinHeap(block));
				writeBlockToTape(tapes[blockNum++%p],HeapSort.reverseArray(heap.getHeapArray()));
			}
			if(lastElem)
				endOfInput = true;
			if(tapes[p].headPointer == Tape.RIGHT_EDGE)
				lastElem = true;
		}
	}

	/*
	 * The replacement selection form of the A phase of the the external sorting algorithm.
	 */
	public static void phaseAReplacementSelection(Tape[] tapes) throws Exception {
		MinHeap heap = new MinHeap(M);
		tapes[p].roll(tapes[p].headPointer);
		for(int i=0; i<M; i++) {
			heap.insert(tapes[p].readHead());
			totalNumberOfMemoryActions++;		// read an element from tape to memory
		}
		int numOfNodes;
		int last;
		int indexOffset = p;
		boolean lastElem = false;
		boolean endOfInput = false;
		numOfNodes = heap.getNumOfNodes();
		while(tapes[p].headPointer <= Tape.RIGHT_EDGE) {
			heap.createMinHeap();
			last = numOfNodes-1;
			int nextElem;
			while(last > -1 && tapes[p].headPointer <= Tape.RIGHT_EDGE) {
				nextElem = tapes[p].readHead();
				totalNumberOfMemoryActions++;		// read an element from tape to memory
				int heapMin = heap.returnMin();
				tapes[p-indexOffset].writeHead(heapMin);
				totalNumberOfMemoryActions++;		// wrote an element from memory to tape
				if(nextElem >= heapMin) {
					heap.setElement(0, nextElem);
					heap.restoreHeapProperty(0);
				}
				else {
					heap.setElement(0, heap.getHeapArray()[last]);
					heap.setElement(last, nextElem);
					heap.setNumOfNodes(heap.getNumOfNodes()-1);
					last--;
					heap.restoreHeapProperty(0);
				}
				if(lastElem) {
					endOfInput = true;
					break;
				}
				if(tapes[p].headPointer == Tape.RIGHT_EDGE)
					lastElem = true;
			}
			tapes[p-indexOffset].addBlockEndPoint(tapes[p-indexOffset].headPointer-1);
			if(endOfInput)
				break;
			if(--indexOffset == 0)
				indexOffset = p;
		}
		if(heap.getNumOfNodes() < M)
			heap.createMinHeap();
		if(--indexOffset == 0)
			indexOffset = p;
		while(heap.getNumOfNodes() > 0) {
			tapes[p-indexOffset].writeHead(heap.deleteMin());
			totalNumberOfMemoryActions++;		// wrote an element from memory to tape
		}
		tapes[p-indexOffset].addBlockEndPoint(tapes[p-indexOffset].headPointer-1);
	}

	/*
	 * The B phase of the the external sorting algorithm.
	 */
	public static void phaseB(Tape[] tapes) throws Exception {
		boolean endOfRun = false;
		int indexOffset = 0;
		boolean done = false;
		//int c = 1;
		while(!done) {
			//System.out.println("\n~~~~~~~~~~~~~~~~ c = "+(c++)+" ~~~~~~~~~~~~~~~~~\n");
			totalNumberOfPasses++;
			fullyRollTapes(tapes, 0+indexOffset, p-1+indexOffset); 			// fully roll input tapes
			fullyRollTapes(tapes, p-indexOffset, numOfTapes-1-indexOffset); // fully roll output tapes
			for(int i=p-indexOffset; i<=numOfTapes-1-indexOffset; i++)		// erase blocks end points of output tapes
				tapes[i].eraseBlocksEndPoints();
			int numOfRuns = tapes[0+indexOffset].blocksEndPoints.size();
			//System.out.println("numOfRuns = "+numOfRuns);
			for(int k=0; k<numOfRuns; k++) {
				//System.out.println("\n~~~~~~~~~~~~~~~~~~~~~ run #"+(k+1)+" ~~~~~~~~~~~~~~~~~~~~~~\n");
				totalNumberOfRuns++;
				MinHeap heap = new MinHeap(0);
				// read elements from tapes to memory
				Vector<Integer[]> vec = new Vector<Integer[]>(p);
				Integer[] values;
				int numOfElements = 0;
				for(int i=0; i<p; i++) {
					if(tapes[i%p+indexOffset].blocksEndPoints.isEmpty())	// nothing has been written to this tape
						continue;
					if(!tapes[i%p+indexOffset].blocksEndPoints.isEmpty()) {
						numOfElements = tapes[i%p+indexOffset].blocksEndPoints.lastElement().intValue()+1;
						if(tapes[i%p+indexOffset].headPointer == numOfElements)
							continue;
					}
					int elem = tapes[i%p+indexOffset].readHead();
					//System.out.println("Read elem "+elem+" from tape"+(i%p+indexOffset+1));
					values = new Integer[3];
					values[0] = elem;
					values[1] = i%p+indexOffset;
					values[2] = tapes[i%p+indexOffset].headPointer;		// next element position
					vec.addElement(values);
					heap.insert(elem);
					totalNumberOfMemoryActions++;		// read an element from tape to memory
				}
				//int m = 1;
				while(heap.getNumOfNodes() > 0) {
					//System.out.println("\n~~~~~~~~~~~~~~~~ m = "+(m++)+" ~~~~~~~~~~~~~~~~~\n");
					int min = heap.deleteMin();
					tapes[p+(k%p)-indexOffset].writeHead(min);
					//System.out.println("Wrote element "+min+" to tape "+(p+(k%p)-indexOffset+1));
					totalNumberOfMemoryActions++;		// wrote an element from memory to tape
					Integer[] val = new Integer[3];
					for(int i=0; i<vec.size(); i++) {
					   Integer[] v = vec.elementAt(i);
					   if(v[0] == min) {
						   val = vec.remove(i);
						   break;
					   }
					}
				    for(Enumeration<Integer> e = tapes[val[1]].blocksEndPoints.elements(); e.hasMoreElements();) {
				    	int pointer = e.nextElement().intValue();
						if(val[2]-1 == pointer) {
							//System.out.println("endOfRun, continuing...");
							endOfRun = true;
							break;
						}
				    }
				    if(endOfRun) {
				    	endOfRun = false;
				    	//m++;
				    	continue;
				    }
					tapes[val[1]].roll(tapes[val[1]].headPointer-val[2]);
					int next_elem = tapes[val[1]].readHead();
					values = new Integer[3];
					values[0] = next_elem;
					values[1] = val[1];
					values[2] = tapes[val[1]].headPointer;
					vec.addElement(values);
					heap.insert(next_elem);
				}
				int headPointer = tapes[p+(k%p)-indexOffset].headPointer;
				if(headPointer == Tape.RIGHT_EDGE) {
					done = true;
					headPointer++;
				}
				tapes[p+(k%p)-indexOffset].addBlockEndPoint(headPointer-1);
			}
			indexOffset = (indexOffset == 0) ? p : 0;
			//printTapes(tapes);
		}
	}

	/*
	 * This method writes the given block (array) to the tape given.
	 */
	public static void writeBlockToTape(Tape tape, int[] block) {
		for(int i=0; i<block.length; i++) {
			tape.writeHead(block[i]);
			totalNumberOfMemoryActions++;		// wrote an element from memory to tape
		}
		tape.addBlockEndPoint(tape.headPointer-1);
	}

	/*
	 * This method fully roll the tapes given (by the two arguments,
	 * startTape and endTape) until the head of each tape reach its
	 * left edge.
	 */
	public static void fullyRollTapes(Tape[] tapes, int startTape, int endTape) {
		for(int i=startTape; i<=endTape; i++)
			tapes[i].roll(tapes[i].headPointer);
	}

	/*
	 * Calculates the total number of actions performed by the tapes by
	 * summing the actions of each tape.
	 */
	public static void calculateTotalNumOfTapeActions(Tape[] tapes) {
		for(int i=0; i<numOfTapes; i++)
			totalNumberOfTapeActions += tapes[i].numOfActionsPerformed;
	}

	/*
	 * This method print the contents of all tapes.
	 */
	public static void printTapes(Tape[] tapes) {
		for(int i=0; i<numOfTapes; i++) {
			System.out.println("\ntape #"+(i+1));
			int numOfElements = 0;
			if(!tapes[i].blocksEndPoints.isEmpty())
				numOfElements = tapes[i].blocksEndPoints.lastElement().intValue()+1;
//			System.out.println("number of written elements = "+numOfElements);
			for(int t=0; t<Tape.TAPE_LENGTH; t++) {
				if(t < numOfElements)
					System.out.print(tapes[i].tapeArray[t]+" ");
				else
					System.out.print("# ");
			}
//			System.out.print("\npointers are: ");
//		    for(Enumeration<Integer> e = tapes[i].blocksEndPoints.elements(); e.hasMoreElements();) {
//		    	System.out.print(e.nextElement()+" ");
//		    }
//		    System.out.println("\nnumber of actions performed = "+tapes[i].numOfActionsPerformed);
		}
		System.out.println("\n\n");
	}
}
