package partA.tape;

import java.util.Random;
import java.util.Vector;

/*
 * This class represents a magnetic tape. It has methods for
 * creating a new magnetic tape of given size, rolling and
 * unrolling the tape, read an element from the the position
 * that the tape's head is pointing and also write an element
 * to the position that the tape's head is pointing. Internally
 * the tape is represented by an array.
 */
public class Tape {

	static int TAPE_LENGTH;				// the length of the tape
	static int LEFT_EDGE;					// the leftmost edge the tape
	static int RIGHT_EDGE;					// the rightmost edge of the tape
	int[] tapeArray;						// the array that represents the tape
	int headPointer;						// the head of the tape
	Vector<Integer> blocksEndPoints;		// stores the end points of all blocks of the tape
	int numOfActionsPerformed;				// total number of actions the tape performs

	/*
	 * Constructor. Creates an empty tape with the size given. If for
	 * any reason (no memory) the array can't be created, the array is
	 * set to null and an exception is thrown.
	 */
	public Tape(int max_size) {
		try {
			TAPE_LENGTH = max_size;
			LEFT_EDGE = 0;
			RIGHT_EDGE = TAPE_LENGTH-1;
			tapeArray = new int[max_size];
			headPointer = LEFT_EDGE;
			blocksEndPoints = new Vector<Integer>();
			numOfActionsPerformed = 0;
		} catch (Exception e) {
			System.out.println("Error: Can't create tape!");
			e.printStackTrace();
		}
	}

	/*
	 * This method just erases the stored end points of
	 * all blocks of the tape.
	 */
	public void eraseBlocksEndPoints() {
		blocksEndPoints = new Vector<Integer>();
	}

	/*
	 * Adds a new end point for the current block.
	 */
	public void addBlockEndPoint(int endPoint) {
		blocksEndPoints.add(endPoint);
	}

	/*
	 * Rolls the tape by one position (to the left = towards its LEFT_EDGE).
	 */
	public void roll() {
		simulateTimeDelay();
		numOfActionsPerformed++;
		if(headPointer == LEFT_EDGE)
			System.out.println("Can't roll tape beyond its left edge!");
		else
			headPointer--;
	}

	/*
	 * Rolls the tape by n positions (to the left = towards its LEFT_EDGE).
	 */
	public void roll(int n) {
		simulateTimeDelay();
		numOfActionsPerformed++;
		headPointer -= n;
		if(headPointer < LEFT_EDGE) {
			System.out.println("Can't roll tape beyond its left edge!");
			headPointer = LEFT_EDGE;
		}
	}

	/*
	 * Unrolls the tape by 1 position (to the right = towards its RIGHT_EDGE).
	 */
	public void unroll() {
		simulateTimeDelay();
		numOfActionsPerformed++;
		if(headPointer == RIGHT_EDGE)
			System.out.println("Can't unroll tape beyond its right edge!");
		else
			headPointer++;
	}

	/*
	 * Unrolls the tape by n positions (to the right = towards its RIGHT_EDGE).
	 */
	public void unroll(int n) {
		simulateTimeDelay();
		numOfActionsPerformed++;
		headPointer += n;
		if(headPointer > RIGHT_EDGE) {
			System.out.println("Can't unroll tape beyond its right edge!");
			headPointer = RIGHT_EDGE;
		}
	}

	/*
	 * Reads and returns the element that the tape's head is
	 * pointing to and moves the head to the right (if possible)
	 * by 1 position (unroll).
	 */
	public int readHead() {
		simulateTimeDelay();
		numOfActionsPerformed++;
		try {
			return tapeArray[headPointer];
		} finally {
			if(headPointer < RIGHT_EDGE)
				unroll();
		}
	}

	/*
	 * Writes the given element to the position the tape's head is
	 * pointing to and moves the head to the right (if possible)
	 * by 1 position (unroll).
	 */
	public void writeHead(int value) {
		simulateTimeDelay();
		numOfActionsPerformed++;
		tapeArray[headPointer] = value;
		if(headPointer < RIGHT_EDGE)
			unroll();
	}

	/*
	 * Returns a pseudorandom, uniformly distributed int value
	 * in the range 1-3 (inclusive).
	 */
	public static int getNextIntFromOneToThree() {
		Random random = new Random();
		int sec = random.nextInt(4);
		return sec%3+1;
	}

	/*
	 * Simulates the time that it takes the tape to perform
	 * an action. Waits for 1-3 msec.
	 */
	public static void simulateTimeDelay() {
		try {
			Thread.sleep(getNextIntFromOneToThree());
		}
		catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
