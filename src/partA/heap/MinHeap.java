package partA.heap;

/*
 * This class represents a min-heap object. It has all the methods required for
 * manipulating the heap, such as: creating a heap, restoring the heap property,
 * inserting a new element (integer) in the heap, deleting the minimum element
 * of the heap etc.
 */
public class MinHeap {

	private int[] heapArray;				// the array that represents the heap
	private int numOfNodes;				// the number of nodes of the heap
	private long numOfComparisons = 0;		// total number of comparisons

	/*
	 * First constructor. Takes one argument (int) as the max size for the heap
	 * and creates a new heap array with the given size, that represents the heap.
	 * If for any reason (no memory) the array can't be created, the array is set
	 * to null and an exception is thrown.
	 */
	public MinHeap(int max_size) {
		try {
			heapArray = new int[max_size];
			numOfNodes = 0;
		} catch (Exception e) {
			System.out.println("Error: Can't create heap!");
			e.printStackTrace();
		}
	}

	/*
	 * Second constructor. Takes an array filled with integers as argument,
	 * creates a new heap from it and stores it at the private field heapArray.
	 */
	public MinHeap(int[] newArray) {
		heapArray = newArray.clone();
		numOfNodes = newArray.length;
		createMinHeap();
	}

	/*
	 * Returns the private field heapArray, that represents the heap.
	 */
	public int[] getHeapArray() {
		return heapArray;
	}

	/*
	 * Sets the number of nodes of heap. Used in the heap sort algorithm.
	 */
	public void setNumOfNodes(int numOfNodes) {
		this.numOfNodes = numOfNodes;
	}

	/*
	 * Returns the number of nodes of the heap.
	 */
	public int getNumOfNodes() {
		return numOfNodes;
	}

	/*
	 * Returns the number of comparisons.
	 */
	public long getNumOfComparisons() {
		return numOfComparisons;
	}

	/*
	 * Sets the node (element) of the heap (heapArray) at the specified index
	 * to the value given.
	 */
	public void setElement(int index, int value) {
		heapArray[index] = value;
	}

	/*
	 * This method creates a heap from an array in random state.
	 */
	public void createMinHeap() {
		numOfNodes = heapArray.length;
		int middle = (int) Math.floor(heapArray.length/2);
		for(int i=middle; i>=0; i--)
			restoreHeapProperty(i);
	}

	/*
	 * This method restores the heap property of the heap. Starts at the
	 * index (of the heapArray) given and going down the heap restoring the
	 * heap property along the way. Every subtree below the specified index
	 * must be a heap;
	 */
	public void restoreHeapProperty(int index) {
		int index_of_min;
		int left = left(index);
		int right = right(index);
		numOfComparisons++;
		if(left > 0 && left < numOfNodes && heapArray[left] < heapArray[index])
			index_of_min = left;
		else
			index_of_min = index;
		numOfComparisons++;
		if(right > 0 && right < numOfNodes && heapArray[right] < heapArray[index_of_min])
			index_of_min = right;
		int temp;
		if(index_of_min != index) {
			temp = heapArray[index];
			heapArray[index] = heapArray[index_of_min];
			heapArray[index_of_min] = temp;
			restoreHeapProperty(index_of_min);
		}
	}

	/*
	 * This method inserts the value given at the end of the heap (heapArray)
	 * and restores the heap property afterwards.
	 */
	public void insert(int value) {
		numOfNodes++;
		if(numOfNodes-1 == heapArray.length) {
			int[] tempArray = heapArray;
			heapArray = new int[numOfNodes];
			System.arraycopy(tempArray, 0, heapArray, 0, tempArray.length);
		}
		heapArray[numOfNodes-1] = value;
		int index = numOfNodes-1;
		int temp;
		int parent_index;
		while(index > 0 && heapArray[index] < heapArray[parent(index)]) {
			parent_index = parent(index);
			temp = heapArray[index];
			heapArray[index] = heapArray[parent_index];
			heapArray[parent_index] = temp;
			index = parent_index;
		}
	}

	/*
	 * Returns the minimum value of the heap. Because we have a min-heap
	 * that value is the root of the heap.
	 */
	public int returnMin() {
		return heapArray[0];
	}

	/*
	 * This method deletes the minimum value of the heap (root) and restores
	 * the heap property afterwards.
	 */
	public int deleteMin() throws Exception {
		if(numOfNodes == 0)
			throw new Exception("Can't delete min, heap size is 0!");
		int min = heapArray[0];
		heapArray[0] = heapArray[numOfNodes-1];
		int[] tempArray = heapArray;
		numOfNodes--;
		heapArray = new int[numOfNodes];
		System.arraycopy(tempArray, 0, heapArray, 0, heapArray.length);
		restoreHeapProperty(0);
		return min;
	}

	/*
	 * Returns the index of the parent (inside the heap) of the node given.
	 */
	private int parent(int node) {
		int parent_index = -1;
		if(node > 0 && node-1 < heapArray.length)
			parent_index = (int) Math.floor((node-1)/2);
		return parent_index;
	}

	/*
	 * Returns the index of the left child (inside the heap) of the (parent)
	 * node given.
	 */
	private int left(int node) {
		int left_child_index = -1;
		if(2*node+1 < heapArray.length)
			left_child_index = 2*node+1;
		return left_child_index;
	}

	/*
	 * Returns the index of the right child (inside the heap) of the (parent)
	 * node given.
	 */
	private int right(int node) {
		int right_child_index = -1;
		if(2*node+2 < heapArray.length)
			right_child_index = 2*node+2;
		return right_child_index;
	}
}
