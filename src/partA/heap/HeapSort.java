package partA.heap;

import java.util.Random;

/*
 * This class is used to test the min-heap objects of the class MinHeap.
 * It sorts an array with random integers using a min-heap (object) and
 * a heap sort algorithm.
 */
public class HeapSort {

	/*
	 *  main method
	 */
	public static void main(String[] args) {
		for(int n=10; n<=1000000; n*=10) {
			int[] unsortedArray = createArrayWithRandomIntegers(n);
			System.out.println("********************************************************");
			System.out.println("Number of integers (N) to be sorted = "+unsortedArray.length);
			long theoreticalComparisons = (long) (2*n*Math.floor((Math.log10(n)/Math.log10(2))));
			System.out.println("Theoretical worst-case complexity <= 2*N*floor(log2(N))\n"+
					"2*N*floor(log2(N)) = "+theoreticalComparisons);
			unsortedArray = reverseArray(sortArrayAndPrintOutput(unsortedArray, "Random"));
			sortArrayAndPrintOutput(unsortedArray, " Worst");
			System.out.println("********************************************************\n\n");
		}
	}

	/*
	 * Sorts the array given and prints it before and after sorting with additional info
	 * for the number of comparisons and the time elapsed for the sorting to be done.
	 */
	public static int[] sortArrayAndPrintOutput(int[] unsortedArray, String str) {
		System.out.println("--------------------- "+str+"-Case ----------------------");
		MinHeap heap = new MinHeap(unsortedArray);
		// sort the heap
		long startTimeMs = System.currentTimeMillis();
		heap = heapSort(heap);
		long taskTimeMs = System.currentTimeMillis() - startTimeMs;
		int[] sortedArray = heap.getHeapArray();
		long numOfComparisons = heap.getNumOfComparisons();
		// uncomment only for small number of integers (<=100)
//		System.out.print("Unsorted integers:\t\t\t");
//		for (int i=0; i<unsortedArray.length; i++) {
//			System.out.print(unsortedArray[i] + " ");
//		}
//		System.out.println();
		// uncomment only for small number of integers (<=100)
//		System.out.print("Sorted (in descending order) integers:  ");
//		for (int i=0; i<sortedArray.length; i++) {
//			System.out.print(sortedArray[i] + " ");
//		}
//		System.out.println();
		System.out.println("Number of comparisons = "+numOfComparisons);
		System.out.println("Time elapsed (in msec) = "+taskTimeMs);
		return sortedArray;
	}

	/*
	 * Reverses an array. Used in the worst-case complexity when an
	 * array sorted in ascending order must be sorted in descending order
	 * and opposite.
	 */
	public static int[] reverseArray(int[] array) {
		int[] reversedArray = new int[array.length];
		for(int i = 0; i < array.length; i++) {
			reversedArray[i] = array[array.length-1-i];
		}
		return reversedArray;
	}

	/*
	 * Creates and fills an array with random integers, in the range
	 * 0(inclusive)-100(exclusive) and returns it.
	 */
	public static int[] createArrayWithRandomIntegers(int size) {
		int[] array = new int[size];
		int range = 100;
		Random random = new Random();
		for(int i = 0; i < array.length; i++) {
			array[i] = random.nextInt(range);
		}
		return array;
	}

	/*
	 * Heap sort algorithm. Takes a min-heap object as an
	 * argument and sort the array the represents the heap (heapArray)
	 * in descending order. First element of the sorted array
	 * (heapArray(0)) is the max element. Finally the (processed)
	 * min-heap object is returned.
	 */
	public static MinHeap heapSort(MinHeap newHeap) {
		int[] heapArray = newHeap.getHeapArray();
		int temp;
		for(int i=heapArray.length-1; i>0; i--) {
			temp = heapArray[0];
			heapArray[0] = heapArray[i];
			heapArray[i] = temp;
			newHeap.setNumOfNodes(newHeap.getNumOfNodes()-1);
			newHeap.restoreHeapProperty(0);
		}
		return newHeap;
	}
}
